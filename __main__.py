#!/usr/bin/env python3

import lxml.html
import requests
import os
import sys
import logging
import getpass


CHECK_HOST = 'example.org'
PORTAL_URL = 'https://connect.wifirst.net'
CONNECT_URL = 'https://connect.wifirst.net/?perform=true'
PORTAL_SELFCARE_URL = 'https://selfcare.wifirst.net/sessions/new'
CONNECTED_URL = 'https://apps.wifirst.net/'
TIMEOUT = 6
COOKIEJAR_PATH = '/var/tmp/wifirst-connect.cookies'

ROPTS = {'timeout': TIMEOUT}


class InvalidCredentials(Exception):
    pass


class ProtocolChanged(Exception):
    pass


def check_need_connect(s):
    try:
        r = s.get('http://{}/'.format(CHECK_HOST), **ROPTS)
    except requests.exceptions.Timeout:
        return True
    return r.history and r.url.startswith(PORTAL_URL)


def connect(s, email, pswd):
    try:
        logging.info("Getting selfcare auth form")
        r = requests.get(CONNECT_URL, **ROPTS)
        assert r.history
        assert r.url.startswith(PORTAL_SELFCARE_URL)

        doc = lxml.html.fromstring(r.content)
        doc.make_links_absolute(r.url)
        form = doc.xpath('//form[contains(@action, "session")]')[0]
        data = dict(form.form_values())
        assert 'login' in data
        assert 'password' in data
        data['login'] = email
        data['password'] = pswd

        logging.info("Authenticating")
        r = s.post(form.action, data, **ROPTS)
        if not r.history:
            logging.error("Invalid credentials")
            raise InvalidCredentials()

        logging.info("Credentials are valid, getting intermediary connect form")
        r = s.get(CONNECT_URL)

        doc = lxml.html.fromstring(r.content)
        doc.make_links_absolute(r.url)
        assert doc.forms
        form = doc.forms[0]
        data = dict(form.form_values())
        data['qos_class'] = '1'  # lol.

        logging.info("Connecting")
        r = s.post(form.action, data, **ROPTS)
        assert r.history
        assert r.url.startswith(CONNECTED_URL)
        logging.info("Success")
        return

    except AssertionError:
        raise ProtocolChanged()
    raise ProtocolChanged()


if __name__ == '__main__':
    import argparse
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(description="Connect to Wifirst captive portal")
    parser.add_argument("-c", "--cookie", action="store_true", help="use cookie jar stored at {}".format(COOKIEJAR_PATH))
    parser.add_argument("-C", "--no-cookie", action="store_true", default=True, help="prevent using cookie jar stored at {} [default]".format(COOKIEJAR_PATH))
    parser.add_argument("email", help="login email")
    parser.add_argument("password", nargs="?", default=None, help="plaintext password; leave empty for an interactive prompt; you can also use WIFIRST_PSWD environment variable")

    args = parser.parse_args()
    use_cookie = args.cookie and not args.no_cookie

    s = requests.Session()

    if use_cookie:
        cookiejar = requests.compat.cookielib.LWPCookieJar(COOKIEJAR_PATH)
        s.cookies = cookiejar
        try:
            s.cookies.load(ignore_discard=True)
            logging.info("Cookies loaded from {}".format(COOKIEJAR_PATH))
        except OSError:
            pass

    s.headers['User-Agent'] = 'Mozilla/5.0 (compatible)'

    if not check_need_connect(s):
        logging.info("Already connected")
        sys.exit(0)

    logging.info("Connection needed")

    if args.password:
        pswd = args.password
    else:
        pswd = os.environ.get('WIFIRST_PSWD', getpass.getpass())

    connect(s, args.email, pswd)
    logging.info("Connected")

    if use_cookie:
        s.cookies.save(ignore_discard=True)
        s.debug("Cookies saved at {}".format(COOKIEJAR_PATH))

    sys.exit(0)

